import 'dart:async';
import 'package:flutter/material.dart';
import 'package:hdsl/log_in_page.dart';
 
 class SplashScreen extends StatefulWidget {
   const SplashScreen({ Key? key }) : super(key: key);

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Timer(Duration(seconds:5), (){
      Navigator.push(context , MaterialPageRoute(builder: (context) => const LogInPage()));
    });
  }
   @override
   Widget build(BuildContext context) {
     return Scaffold(

       body: Container(
         constraints: BoxConstraints.expand(),
         child: Column(
           mainAxisAlignment: MainAxisAlignment.center,
           crossAxisAlignment: CrossAxisAlignment.center,
           children: [
            Image.asset('assets/images/armchair.png'),
           Text(' Furniture')
           ],),

       ),

     );
   }
}
