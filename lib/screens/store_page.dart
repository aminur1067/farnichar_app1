import 'package:flutter/material.dart';
import 'package:hdsl/screens/cart_page.dart';
import 'package:hdsl/screens/home_page.dart';
import 'package:hdsl/screens/profile_page.dart';
import 'package:hdsl/screens/wistlist_page.dart';

class StorePage extends StatefulWidget {
  const StorePage({ Key? key }) : super(key: key);

  @override
  _StorePageState createState() => _StorePageState();
}

class _StorePageState extends State<StorePage> {
  List pages=[const HomePage(),const CartPage(), CompleteProfileForm(),const WishlistPage(),];
  int index =0;
  

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: pages[index],
      bottomNavigationBar:BottomNavigationBar(
        
        currentIndex:index ,
        onTap: (value){
          index=value;
          setState(() {
            
          });
        },
        type: BottomNavigationBarType.fixed,

        items:const [
        
        BottomNavigationBarItem(icon: Icon(Icons.home), label:'home'),
        BottomNavigationBarItem(icon: Icon(Icons.favorite), label:'Wistlist'),
        BottomNavigationBarItem(icon: Icon(Icons.shopping_cart), label:'Cart'),
        BottomNavigationBarItem(icon: Icon(Icons.person), label:'Profile'),
      ]) ,
      
      
    );
  }
}

