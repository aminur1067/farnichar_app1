import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'cart_page.dart';
import 'datails_page.dart';
class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  List Catagory = ['Top item', 'Almira', 'Tabile', 'Alna', 'Chair'];
  List Catagory2 = ['top item', 'almira', 'tabile', 'alna', 'chair'];
  int Selectedindex = 0;

  List item = [];

  @override
  void initState() {
    super.initState();
    getData('top item');
  }

  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white10,
      body: SafeArea(
        child: Container(
          padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                const SizedBox(
                  height: 20,
                ),
                const Text(
                  'Best Furniture',
                  style: TextStyle(fontSize: 25, fontWeight: FontWeight.w600),
                ),
                const SizedBox(
                  height: 10,
                ),
                const Text('Your Best Choice'),
                const SizedBox(
                  height: 10,
                ),
                TextField(
                  decoration: InputDecoration(
                    hintText: 'search here',
                    suffixIcon: const Icon(Icons.search),
                    isDense: true,
                    border: OutlineInputBorder(
                      borderSide:
                          BorderSide(color: Colors.grey.shade100, width: 0.5),
                      borderRadius: BorderRadius.circular(24),
                    ),
                  ),
                ),
                Container(
                  margin: const EdgeInsets.symmetric(vertical: 10),
                  height: 40,
                  child: ListView.separated(
                    separatorBuilder: (context, index) {
                      return const SizedBox(
                        height: 10,
                      );
                    },
                    itemCount: Catagory.length,
                    scrollDirection: Axis.horizontal,
                    itemBuilder: (context, index) {
                      return MaterialButton(
                        shape: const StadiumBorder(),
                        onPressed: () {
                          Selectedindex = index;
                          setState(() {});
                        },
                        minWidth: 100,
                        color: Selectedindex == index ? Colors.black : null,
                        child: Text(
                          Catagory[index],
                          style: TextStyle(
                              color: Selectedindex == index
                                  ? Colors.white
                                  : Colors.black),
                        ),
                      );
                    },
                  ),
                ),
                Expanded(
                    child: item.isEmpty
                        ? const Center(
                            child: CircularProgressIndicator(),
                          )
                        : ListView.separated(
                            itemBuilder: (context, index) {
                              
                              return InkWell(
                                onTap: (){
                                  Navigator.push(context, MaterialPageRoute(builder: (context)=> DetailsPage(ptoductDetails: {},)));
                                },
                                child: Container(
                                  height: 180,
                                  decoration: BoxDecoration(
                                    color: Colors.grey.shade400,
                                    borderRadius: BorderRadius.circular(20),
                                  ),
                                  child: Row(
                                    children: [
                                      Container(
                                        height: 150,
                                        width: 150,
                                        color: Colors.white,
                                        margin: const EdgeInsets.all(20),
                                        padding: const EdgeInsets.all(20),
                                        child:
                                           Image.network(item[index]['images']),
                                      ),
                                      Expanded(
                                        child: Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: [
                                            Text(
                                              '${item[index]['title']}',
                                              style: const TextStyle(
                                                  fontSize: 20,
                                                  color: Colors.blueAccent,
                                                  fontWeight: FontWeight.bold),
                                            ),
                                            Padding(
                                              padding: const EdgeInsets.symmetric(
                                                  vertical: 8.0),
                                              child: Text(
                                                '${item[index]['catagory']}',
                                                style: const TextStyle(
                                                    fontWeight: FontWeight.bold),
                                              ),
                                            ),
                                            Text(
                                              '${item[index]['rating']}',
                                              style: const TextStyle(
                                                  overflow: TextOverflow.ellipsis,
                                                  fontWeight: FontWeight.normal),
                                            ),
                                            const SizedBox(
                                              height: 20,
                                            ),
                                             Text(
                                              '${item[index]['description']}',
                                              style: const TextStyle(
                                                  overflow: TextOverflow.ellipsis,
                                                  fontWeight: FontWeight.normal),
                                            ),
                                             SizedBox(
                                              height: 20,),

                                            Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.spaceBetween,
                                              children: [
                                                Text(
                                                  '৳${item[index]['price']}',
                                                ),
                                                const SizedBox(
                                                  height: 20,
                                                ),
                                                MaterialButton(
                                                  shape: const StadiumBorder(),
                                                  color: Colors.blue,
                                                  onPressed: () {},
                                                  child: const Text(
                                                    'Buy now',
                                                    // style: TextStyle(
                                                    //    backgroundColor: Colors.blue),
                                                  ),
                                                )
                                              ],
                                            )
                                          ],
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                              );
                            },
                            separatorBuilder: (context, index) {
                              return const SizedBox(
                                height: 10,
                              );
                            },
                            itemCount: item.length))
              ]),
        ),
      ),
    );
  }

  getData(
      String keyWord
      ) async {
    FirebaseFirestore firestore = FirebaseFirestore.instance;
    var data = await firestore.collection(keyWord).get();
    for (var doc in data.docs) {
      setState(() {
        Map map = {
          'title': doc['title'],
          'catagory': doc['catagory'],
          'images': doc['images'],
          'price': doc['price'],
          'description':doc['description'],
          'rating': doc['rating'],
        };
        item.add(map);
      });
      print(item);
    }
  }
}
