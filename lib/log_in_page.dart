import 'package:flutter/material.dart';
import 'package:hdsl/screens/store_page.dart';
import 'package:hdsl/sing_up_page.dart';
import 'package:firebase_auth/firebase_auth.dart';

class LogInPage extends StatefulWidget {
  const LogInPage({Key? key}) : super(key: key);

  @override
  _LogInPageState createState() => _LogInPageState();
}

class _LogInPageState extends State<LogInPage> {
  TextEditingController mailcontroller = TextEditingController();
  TextEditingController passwordcontroller = TextEditingController();
  bool isVisible = false;
  @override
  Widget build(BuildContext context) {

    return MaterialApp(
       debugShowCheckedModeBanner: false,
       home: Scaffold(

        body: Container(
          padding: EdgeInsets.symmetric(horizontal: 60),
          constraints: BoxConstraints.expand(),

          // color: Colors.blue,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Image.asset('assets/images/armchair.png'),
              SizedBox(
                height: 13,
              ),
              Text('User log in'),
              SizedBox(
                height: 10,
              ),
              TextField(
                onChanged: (value) {
                  setState(() {});
                },
                controller: mailcontroller,
                decoration: InputDecoration(
                  isDense: true,
                  hintText: 'your mail@gmail.com',
                  prefixIcon: Icon(Icons.mail),
                  suffixIcon: mailcontroller.text.isNotEmpty
                      ? InkWell(
                          onTap: () {
                            mailcontroller.clear();
                          },
                          child: Icon(Icons.clear),
                        )
                      : null,

                  // suffixIcon:mailcontroller.text.isEmpty?const Icon(Icons.clear):null,
                  border:
                      OutlineInputBorder(borderRadius: BorderRadius.circular(35)),
                ),
              ),
              SizedBox(
                height: 16,
              ),
              TextField(
                controller: passwordcontroller,
                obscureText: isVisible,
                decoration: InputDecoration(
                  isDense: true,
                  hintText: 'password',
                  prefixIcon: Icon(Icons.lock),
                  suffixIcon: GestureDetector(
                      onTap: () {
                        isVisible = !isVisible;
                      },
                      child: Icon(
                          isVisible ? Icons.visibility : Icons.visibility_off)),
                  border:
                      OutlineInputBorder(borderRadius: BorderRadius.circular(35)),
                ),
              ),
              SizedBox(
                height: 16,
              ),
              MaterialButton(
                  shape: StadiumBorder(),
                  color: Colors.blue,
                  child: Text('login'),
                  onPressed: () async {
                    print('login button pressed');

                    try {
                      UserCredential userCredential =
                          await FirebaseAuth.instance.signInWithEmailAndPassword(
                        email: mailcontroller.text,
                        password: passwordcontroller.text,
                      );

                      print(userCredential.user);

                      var user = userCredential.user;
                      if (user != null) {
                        Navigator.pushReplacement(
                          context,
                          MaterialPageRoute(
                            builder: (context) => const StorePage(),
                          ),
                        );
                      }
                    } on FirebaseAuthException catch (e) {
                      if (e.code == 'user-not-found') {
                        print('No user found for that email.');
                      } else if (e.code == 'wrong-password') {
                        print('Wrong password provided for that user.');
                      }
                    }
                  }),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text('Dont have a account'),
                  TextButton(
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => SingUpPage(),
                        ),
                      );
                    },
                    child: Text('Create one'),
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
