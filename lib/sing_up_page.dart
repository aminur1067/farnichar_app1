import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

import 'log_in_page.dart';

class SingUpPage extends StatefulWidget {
  const SingUpPage({Key? key}) : super(key: key);

  @override
  _SingUpPageState createState() => _SingUpPageState();
}

class _SingUpPageState extends State<SingUpPage> {
  TextEditingController mailcontroller = TextEditingController();
  TextEditingController passwordcontroller = TextEditingController();
  TextEditingController namecontroller = TextEditingController();
  TextEditingController nambercontroller = TextEditingController();
  bool isVisible = false;
  FirebaseFirestore firestore = FirebaseFirestore.instance;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        padding: EdgeInsets.symmetric(horizontal: 60),
        constraints: BoxConstraints.expand(),

        // color: Colors.blue,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Image.asset(
              'assets/images/armchair.png',
              height: 100,
              width: 100,
            ),
            SizedBox(
              height: 13,
            ),
            Text('User Registetion'),
            SizedBox(
              height: 10,
            ),
            TextField(
              onChanged: (value) {
                setState(() {});
              },
              controller: namecontroller,
              decoration: InputDecoration(
                isDense: true,
                hintText: 'User name',
                prefixIcon: Icon(Icons.person),
                suffixIcon: namecontroller.text.isNotEmpty
                    ? InkWell(
                        onTap: () {
                          namecontroller.clear();
                        },
                        child: Icon(Icons.clear),
                      )
                    : null,

                //suffixIcon:mailcontroller.text.isEmpty?const Icon(Icons.clear):null,
                border:
                    OutlineInputBorder(borderRadius: BorderRadius.circular(35)),
              ),
            ),
            SizedBox(
              height: 16,
            ),
            TextField(
              onChanged: (value) {
                setState(() {});
              },
              controller: mailcontroller,
              decoration: InputDecoration(
                isDense: true,
                hintText: 'your mail',
                prefixIcon: Icon(Icons.mail),
                suffixIcon: mailcontroller.text.isNotEmpty
                    ? InkWell(
                        onTap: () {
                          mailcontroller.clear();
                        },
                        child: const Icon(Icons.clear),
                      )
                    : null,

                // suffixIcon:mailcontroller.text.isEmpty?const Icon(Icons.clear):null,
                border:
                    OutlineInputBorder(borderRadius: BorderRadius.circular(35)),
              ),
            ),
            const SizedBox(
              height: 16,
            ),
            TextField(
              onChanged: (value) {
                setState(() {});
              },
              controller: nambercontroller,
              decoration: InputDecoration(
                isDense: true,
                hintText: 'your Namber',
                prefixIcon: const Icon(Icons.phone),
                suffixIcon: nambercontroller.text.isNotEmpty
                    ? InkWell(
                        onTap: () {
                          nambercontroller.clear();
                        },
                        child: const Icon(Icons.clear),
                      )
                    : null,

                // suffixIcon:mailcontroller.text.isEmpty?const Icon(Icons.clear):null,
                border:
                    OutlineInputBorder(borderRadius: BorderRadius.circular(35)),
              ),
            ),
            const SizedBox(
              height: 16,
            ),
            TextField(
              controller: passwordcontroller,
              obscureText: isVisible,
              decoration: InputDecoration(
                isDense: true,
                hintText: 'password',
                prefixIcon: Icon(Icons.lock),
                suffixIcon: GestureDetector(
                    onTap: () {
                      isVisible = !isVisible;
                    },
                    child: Icon(
                        isVisible ? Icons.visibility : Icons.visibility_off)),
                border:
                    OutlineInputBorder(borderRadius: BorderRadius.circular(35)),
              ),
            ),
            SizedBox(
              height: 16,
            ),
            MaterialButton(
                shape: StadiumBorder(),
                color: Colors.blue,
                child: Text('Sing Up'),
                onPressed: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => LogInPage()));

                  UserSingUp();
                  saveUserdata();
                }),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                const Text('Already have a account'),
                TextButton(
                    onPressed: () {
                      FirebaseFirestore.instance.toString();
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) => LogInPage()));
                    },
                    child: const Text('Sing in')),
              ],
            )
          ],
        ),
      ),
    );
  }

  void UserSingUp() async {
    // FirebaseAuth auth = FirebaseAuth.instance;
    try {
      UserCredential userCredential = await FirebaseAuth.instance
          .createUserWithEmailAndPassword(
              email: mailcontroller.text, password: passwordcontroller.text);
      saveUserdata();
    } on FirebaseAuthException catch (e) {
      if (e.code == 'weak-password') {
        print('The password provided is too weak.');
      } else if (e.code == 'email-already-in-use') {
        print('The account already exists for that email.');
      }
    } catch (e) {
      print(e);
    }
  }

  showErrorDiolog(String ErrorMsg) {
    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text('Werning!'.toUpperCase()),
            content: Text(ErrorMsg),
          );
        });
  }

  saveUserdata() {
    FirebaseFirestore firestore = FirebaseFirestore.instance;
    firestore.collection('User').doc(mailcontroller.text).set({
      'mail': mailcontroller.text,
      'namber': nambercontroller.text,
      'name': namecontroller.text,
      'password': passwordcontroller.text
    });
  }
}
